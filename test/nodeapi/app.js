var express=require('express');
var bodyparser=require('body-parser');
var promise = require('bluebird');
var fs = require('fs');
var path = require('path');
var app = express();
app.use(express.static(path.resolve(__dirname,"public")));
app.use(bodyparser.json({limit:"5mb"}))
app.use(bodyparser.urlencoded({
    limit:"2mb",
    extended:true
}))
var opt = {
    promiseLib: promise
}
var pgp = require('pg-promise')(opt)

var cs= "postgres://postgres:root1@localhost:5432/angular";
var db=pgp(cs)
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin",'*');
    res.setHeader("Access-Control-Allow-Methods",'*');
    res.setHeader("Access-Control-Allow-Headers",'X-Requested-With,content-type');
    res.setHeader("Access-Control-Allow-Credentails",true);
    next();
})


app.post('/add', (req, res, next) => {
    var name=req.body.productname;
    var productprice=req.body.productprice;
    db.any("select * from fn_addproduct($1,$2)",[name,productprice]).then((data)=>{
        if (data.length > 0) {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: data,
                message: "Posted Successfully"
            })
        }
        else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "Posted Successfully"
            })
        }
    }).catch(error => {
        res.status(200).send({
            result: false,
            error: error.message,
            data: "ERROR",
            message: "Unable to Post Products "
        })
    })
})

app.get('/getAllProducts', (req, res, next) => {
    db.any("select * from view_products").then((data)=>{
        if (data.length > 0) {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: data,
                message: "Products Retrived Successfully"
            })
        }
        else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "Products Retrived Successfully"
            })
        }
    }).catch(error => {
        res.status(200).send({
            result: false,
            error: error.message,
            data: "ERROR",
            message: "Products Retrived UnSuccessfully "
        })
    })
})

app.get('/getProductsById/:Id', (req, res, next) => {
    var id=req.params.Id
    db.any("select * from fn_getProductsById($1)",id).then((data)=>{
        if (data.length > 0) {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: data,
                message: "Products Retrived Successfully"
            })
        }
        else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "Products Retrived Successfully"
            })
        }
    }).catch(error => {
        res.status(200).send({
            result: false,
            error: error.message,
            data: "ERROR",
            message: "Unable to Retrive Products "
        })
    })
})




app.post('/updateProduct/:Id', (req, res, next) => {
    var name=req.body.productname;
    var productprice=req.body.productprice;
    var id=req.params.Id;
    db.any("select * from public.fn_updateproducts($1,$2,$3)",[id,name,productprice]).then((data)=>{
        if (data.length > 0) {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: data,
                message: "Product Updated Successfully"
            })
        }
        else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "Product Updated Successfully"
            })
        }
    }).catch(error => {
        res.status(200).send({
            result: false,
            error: error.message,
            data: "ERROR",
            message: "Unable to Update Products "
        })
    })
})

app.listen(3400, (err) => {
    if (err)
        console.log("Server cannot start");
    else
        console.log("Server Started at http://localhost:3400")
})
