import { Component, OnInit, ViewChild } from '@angular/core';
import { Products } from '../model/products';
import { ServiceService } from '../services/service.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
@ViewChild('addfrm') form:any
  products:any
  productsList: any;
  productById: any;
  show: boolean;
  constructor(private se:ServiceService) { 
    this.products=new Products();
  }

  addProduct()
  {
    
    this.se.saveProduct(this.products).subscribe((data)=>{
      this.getAllProducts();
      this.form.reset();
    })
  }
  ngOnInit() {
   this.getAllProducts()
   this.show=true;
  }
  getAllProducts()
  {
    this.se.getProducts().subscribe((data)=>{
      if (data.result && data.data != 'NDF') {
        this.productsList=data.data;
      }
    })
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray( this.productsList, event.previousIndex, event.currentIndex);
  }
  getbyId(id:number)
  {
    this.show=false
this.se.getProductsById(id).subscribe((data)=>{
  if (data.result && data.data != 'NDF') {
    this.products.id=data.data[0].id;
    this.products.productname=data.data[0].productname;
    this.products.productprice=data.data[0].productprice;
  }
})
  }
  updateProduct(id:number)
  {
this.se.updateProduct(id,this.products).subscribe((data)=>{
  this.getAllProducts()
  this.form.reset();
  this.show=true;
})
  }

}
