import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Products } from '../model/products';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  url: string ="http://localhost:3400";
  constructor(private http: HttpClient) { }
  saveProduct(products: Products): Observable<any> {
    return this.http.post(this.url + '/add', JSON.stringify(products), this.httpOptions)
  }
  getProducts(): Observable<any> {
    return this.http.get(this.url + '/getAllProducts', { responseType: 'json' })
  }
  getProductsById(id:number): Observable<any> {
    return this.http.get(this.url + '/getProductsById/'+id, { responseType: 'json' })
  }
  updateProduct(id:number,products: Products): Observable<any> {
    return this.http.post(this.url + '/updateProduct/'+id, JSON.stringify(products), this.httpOptions)
  }
}
